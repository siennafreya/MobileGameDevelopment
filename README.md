# MobileGameDevelopment

#References
Solo Game Dev. (2023) Unity Ads Tutorial - Easy Integration (2023). Available from: https://www.youtube.com/watch?v=6jQIeZk72cA. [Accessed: 17/01/24].
French, J. (2023) How to spawn an object in Unity (using Instantiate). Available from: https://gamedevbeginner.com/how-to-spawn-an-object-in-unity-using-instantiate/. [Accessed: 17/01/2024].
Unity. (no date) Initializing the SDK in unity. Available from: https://docs.unity.com/ads/en-us/manual/InitializingTheUnitySDK. [Accessed: 17/01/24]
Unity. (no date) Implementing banner ads in Unity. Available from: https://docs.unity.com/ads/en-us/manual/ImplementingBannerAdsUnity. [Accessed: 18/01/24]
Unity. (no date) Implementing interstitial ads in Unity. Available from: https://docs.unity.com/ads/en-us/manual/ImplementingBasicAdsUnity. [Accessed: 17/01/24]
elekvault. (2023) MobileWaterShader_EKV. Available from: https://assetstore.unity.com/packages/vfx/shaders/substances/mobilewatershader-ekv-234278. [Accessed: 17/01/24]
Hrekov. (2021) 2D Hand drawing MUSHROOM icons. Available from: https://assetstore.unity.com/packages/2d/gui/icons/2d-hand-drawing-mushroom-icons-200726. [Accessed: 17/01/24]
LowlyPoly. (2022) Stylized Grass texture. Available from: https://assetstore.unity.com/packages/2d/textures-materials/glass/stylized-grass-texture-153153. [Accessed: 17/01/24]
Ladymito. (2020) Free chibi cat. Available from: https://assetstore.unity.com/packages/3d/characters/animals/mammals/free-chibi-cat-165490. [Accessed: 17/01/24]
Synty Studios. (2023) Simple Sky - Cartoon assets. Available from: https://assetstore.unity.com/packages/3d/environments/simple-sky-cartoon-assets-42373. [Accessed: 17/01/24]
Ultrasonic. (2023) Simple, Low-Poly Decorative Plant Assets. Available from: https://assetstore.unity.com/packages/3d/vegetation/simple-low-poly-decorative-plant-assets-252714. [Accessed: 17/01/24]
Gameleon Studios. (2021) Squared Characters - Free Sample. Available from: https://assetstore.unity.com/packages/3d/characters/squared-characters-free-sample-192216. [Accessed: 17/01/24]
Evgenia. (2019) Lowpoly Baker's House. Available from: https://assetstore.unity.com/packages/3d/environments/fantasy/lowpoly-baker-s-house-26443. [Accessed: 17/01/24]
Blue Polygon. (2022) Rope Bridge 3D. Available from: https://assetstore.unity.com/packages/3d/environments/rope-bridge-3d-222563. [Accessed: 17/01/24]
SwishSwoosh. (2023) Free Pop Sound Effects Pack. Available from: https://assetstore.unity.com/packages/audio/sound-fx/free-pop-sound-effects-pack-263821. [Accessed: 18/01/24]
Nox_Sound. (2022) Nature - Essentials. Available from: https://assetstore.unity.com/packages/audio/ambient/nature/nature-essentials-208227. [Accessed: 18/01/24]
Study Tonight. (no date) Unity 3D: Adding Sound Effects to Game. Available from: https://www.studytonight.com/game-development-in-2D/audio-in-unity#:~:text=Unity%203D%3A%20Adding%20an%20Audio%20Source&text=Go%20to%20Add%20Component%20%E2%86%92,mp3. [Accessed: 18/01/24]
French, J. (2022) Make a Follow Camera in Unity (with or without Cinemachine). Available from: https://gamedevbeginner.com/how-to-follow-the-player-with-a-camera-in-unity-with-or-without-cinemachine/. [Accessed: 18/01/24]
Unity Technologies. (2022) Display a rewarded ad with Unity Ads. Available from: https://learn.unity.com/tutorial/unity-monetization-rewarded-ads#62c570c3edbc2a147a085b67. [Accessed: 18/01/24] 