using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    public GameObject Mushroom;
    public float spawnInterval = 1f;
    private float timer = 0f;

    public Vector3 minPosition;
    public Vector3 maxPosition;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // Increment the timer
        timer += Time.deltaTime;

        // Check if it's time to spawn a new mushroom
        if (timer >= spawnInterval)
        {
            // Spawn a mushroom
            SpawnMushroom();

            // Reset the timer
            timer = 0f;
        }
    }
    void SpawnMushroom()
    {
        //sets random position got the mushroom
        Vector3 randomPosition = new Vector3(
            Random.Range(minPosition.x, maxPosition.x),
            Random.Range(minPosition.y, maxPosition.y),
            Random.Range(minPosition.z, maxPosition.z)
        );
        
        Instantiate(Mushroom, randomPosition, Quaternion.identity); //instantiates mushroom
    }
}



