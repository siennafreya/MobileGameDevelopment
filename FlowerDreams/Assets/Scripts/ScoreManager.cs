using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class ScoreManager : MonoBehaviour
{
    public TextMeshProUGUI scoreText;
    public float score;
    public void addMushroom()
    {
        score++; //adds to score
        Debug.Log("Mushroom Added.");
    }

    private void Start()
    {
        UpdateScoreText(); //updates the score text in the ui
    }

    public void UpdateScoreText()
    {
        scoreText.text = score.ToString(); //takes score variable and turns it into a string for text
    }

}
