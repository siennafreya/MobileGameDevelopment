using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragAndDrop : MonoBehaviour
{

    private Touch touch;
    private float speedModifier;

    // Start is called before the first frame update
    void Start()
    {
        speedModifier = 0.01f;
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true; //stops the player from falling over
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Moved)
            {
                transform.position = new Vector3
                    (transform.position.x + touch.deltaPosition.x * speedModifier,
                    transform.position.y - touch.deltaPosition.y * speedModifier,
                    transform.position.z + touch.deltaPosition.y * speedModifier); 
                //transforms player position according to touch position
            }

        }
    }
}
