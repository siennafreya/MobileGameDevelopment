using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour
{
    BoxCollider col;
    public Transform player;
    private AudioSource PopSound;

    private void Start()
    {
        PopSound = GetComponent<AudioSource>();  //finds audio source
        if (PopSound == null)
        {
            Debug.Log("Audio Source is NULL");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Mushroom") //checks to see if player has collided with mushroom
        {
            print("ENTER");
            
            ScoreManager scoreManger = player.GetComponent<ScoreManager>();
            if (scoreManger != null)
            {
                scoreManger.score++; //updates the score
                scoreManger.UpdateScoreText(); //updates the text on the ui with the current score.
            }
            PopSound.Play(); //plays sound effect
            Destroy(other.gameObject); //destroys the mushroom
            
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Mushroom")
        {
            print("STAY");
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Mushroom")
        {
            print("EXIT");
        }
    }

}
