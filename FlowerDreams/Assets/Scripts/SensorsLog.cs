using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensorsLog : MonoBehaviour
{
    private Quaternion correctionQuaternaion;

    // Start is called before the first frame update
    void Start()
    {
        Input.gyro.enabled = true; //enables gyro movement
        Debug.Log("Gyro Enabled");
        correctionQuaternaion = Quaternion.Euler(90f, 0f, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = correctionQuaternaion * GyroToUnity(Input.gyro.attitude); //rotation tranform using gyro input from phone.
    }

    private Quaternion GyroToUnity (Quaternion q)
    {
        return new Quaternion(q.x, q.y, -q.z, -q.w);
    }
}
